import React from "react";

export default function PublicLayout({ children }) {
  return (
    <div>
      This is public layout.
      {children}
    </div>
  );
}
