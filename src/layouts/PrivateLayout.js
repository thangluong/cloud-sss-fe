import React from "react";

export default function PrivateLayout({ children }) {
   const [search, setSearch] = React.useState("");
   return (
      <div>
         This is private layout.
         <input value={search} onChange={(e) => setSearch(e.target.value)} />
         <button onClick={() => console.log(search)}>Search</button>
         {children}
      </div>
   );
}
