import React from "react";
import { Route } from "react-router-dom";

export default function PublicRoute({ component: Component, layout: Layout, ...rest }) {
   return Layout ? (
      <Route
         {...rest}
         render={(props) => (
            <Layout {...props}>
               <Component {...props} />
            </Layout>
         )}
      />
   ) : (
      <Route {...rest} render={(props) => <Component {...props} />} />
   );
}
