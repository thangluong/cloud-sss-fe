import React from "react";
import Profile from "./Profile";

export default function index({ match }) {
   return (
      <Switch>
         <Route exact path={`${match.path}`} component={Profile} />
         <RouteWrapper component={PageNotFound} />
      </Switch>
   );
}
