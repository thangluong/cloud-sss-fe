import React from "react";

export default function Profile() {
   const Foo = () => {
      console.log("HomePage is rendered!");
      const [text, setText] = React.useState("");
      const [isChecked, setIsChecked] = React.useState(false);
      const toggleChecked = React.useCallback(() => setIsChecked(!isChecked), [isChecked]);
      return (
         <>
            <input
               margin="normal"
               variant="outlined"
               label="TextField"
               value={text}
               onChange={(e) => setText(e.target.value)}
            />
            <Bar value={isChecked} onClick={toggleChecked} />
         </>
      );
   };

   const Bar = React.memo(({ value, onClick }) => {
      console.log("Checkbox is rendered!", value);
      return <input type="checkbox" defaultChecked={value} onClick={onClick} />;
   });

   return (
      <div>
         This is a profile.
         <Foo />
      </div>
   );
}
