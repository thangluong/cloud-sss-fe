import React from "react";
import Users from "./Users";
import { Switch, Route } from "react-router-dom";

export default function index({ match }) {
   return (
      <Switch>
         <Route exact path={`${match.path}`} component={Users} />
         {/* <RouteWrapper component={PageNotFound} /> */}
      </Switch>
   );
}
