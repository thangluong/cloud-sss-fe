import React from "react";
import PrivateRoute from "../../common/PrivateRoute";
import UsersIndex from "./users";
import ProfileIndex from "./profile/Profile";
import { Switch } from "react-router-dom";
import PrivateLayout from "../../layouts/PrivateLayout";

export default function index({ match }) {
   return (
      <Switch>
         <PrivateRoute layout={PrivateLayout} path={`${match.path}profile`} component={ProfileIndex} />
         <PrivateRoute layout={PrivateLayout} path={`${match.path}users`} component={UsersIndex} />

         {/* <PublicRoute component={PageNotFound} /> */}
      </Switch>
   );
}
