import React from "react";
import "./App.css";
import { BrowserRouter, Switch } from "react-router-dom";
import PublicRoute from "./common/PublicRoute";
import PublicLayout from "./layouts/PublicLayout";
import Login from "./screens/public/Login";
import Signup from "./screens/public/Signup";
import RootIndex from "./screens/root";

function App() {
  return (
    <BrowserRouter>
      {/* some thing like Auth Context, Root Context, ... */}
      <Switch>
        <PublicRoute exact layout={PublicLayout} path="/" component={Login} />
        <PublicRoute layout={PublicLayout} path="/login" component={Login} />
        <PublicRoute layout={PublicLayout} path="/signup" component={Signup} />

        <PublicRoute path="" component={RootIndex} />
        {/* 404 for the last route */}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
